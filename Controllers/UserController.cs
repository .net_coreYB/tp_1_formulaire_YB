﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Formulaire.Models;

namespace Formulaire.Controllers
{
    public class UserController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Inscription()
        {
            return View();
        }
        public IActionResult Reception(User A)
        {
            if (ModelState.IsValid) {
                // Insertion BD
                // Insertion Session
                // Insertion .txt
                return RedirectToAction("Index","Home"); // Redirection d'une vue grace a l'action formulaire vers un autre controlleur(Home)
            }
            return View("Inscription");
        }
    }
}