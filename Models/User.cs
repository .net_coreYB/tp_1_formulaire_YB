﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Formulaire.Models
{
    public class User
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Votre nom est obligatoire")]
        [RegularExpression("[A-Za-z]{3,30}", ErrorMessage = "Respecter le format Svp !")]
        public string nom { get; set; }
        [Required(ErrorMessage = "Votre Prenom est obligatoire")]
        public string prenom { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DT{ get; set; }
        [EmailAddress(ErrorMessage ="Saisie un mail Valide Svp !")]
        public string email { get; set; }

    }
}
